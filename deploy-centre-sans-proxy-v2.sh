#!/bin/bash

##########################################################
#
# Description : deploiement a la volee de conteneur docker
#
##########################################################

# Si option --create-debian creation de debian seule
if [ "$1" == "--create-debian" ];then

    # definition du nombre de conteneur	
    nbServ=1
    [ "$2" != " " ] && nbServ=$2
    id_first=$(docker ps -a --format '{{ .Names}}' | awk -F "-" -v user=$USER '$0 - user"-debian" {print $3}' | sort -r | head -1)
    id_min=$(($id_first+1))
    # determination de id max
    id_max=$(($nbServ + $id_min - 1))

    # creation des conteneurs
    echo "Debut de la creation du/des conteneurs ..."
    for i in $(seq $id_min $id_max);do
            docker run -tid --cap-add NET_ADMIN --cap-add SYS_ADMIN --publish-all=true -v /srv/data:/srv/html -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name $USER-debian-$i -h $USER-debian-$i registry.gitlab.com/cpen737815/formations_chriski:latest 
            # creation du user en cours
            docker exec -ti $USER-debian-$i /bin/sh -c "useradd -m -p sa3tHJ3/KuYvI $USER"
            # ajout cle publique
            docker exec -ti $USER-debian-$i /bin/sh -c "mkdir ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown $USER:$USER $HOME/.ssh"
            docker cp $HOME/.ssh/id_rsa.pub $USER-debian-$i:$HOME/.ssh/authorized_keys
            docker exec -ti $USER-debian-$i /bin/sh -c "chmod 600 $HOME/.ssh/authorized_keys && chown $USER:$USER $HOME/.ssh/authorized_keys"
	    docker exec -ti $USER-debian-$i /bin/sh -c "echo '$USER  ALL=(ALL)  NOPASSWD: ALL'>>/etc/sudoers"
            docker exec -ti $USER-debian-$i /bin/sh -c "service ssh start"
    done

# Si option --drop
elif [ "$1" == "--drop" ];then

    echo "Suppression des conteneurs ..."
    docker rm -f $(docker ps -a | grep $USER-debian | awk '{print $1}')
    echo "Fin de la suppression."

elif [ "$1" == "--infos" ];then

    echo "Informations des conteneurs :"
    echo ""
    for conteneur in $(docker ps -a | grep $USER-debian | awk '{print $1}');do
	    infos_conteneur=$(docker inspect -f '    => {{.Name}} - {{.NetworkSettings.IPAddress }}' $conteneur)
            echo "${infos_conteneur} - utilisateur : ${USER}"
    done


elif [ "$1" == "--start" ];then

    # Demarrage des conteneurs (et de docker si necessaire)	
    sudo /etc/init.d/docker start

    echo "redemarrage des conteneurs ..."
    docker start $(docker ps -a | grep $USER-debian | awk '{print $1}')
    echo "Conteneurs demarres"


elif [ "$1" == "--ansible" ];then

    echo "Notre option est --ansible"	

elif [ "$1" == "--proxy" ];then

	if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ];then
		echo "Verifier avant /etc/systemd/system/docker.service.d/http-proxy.conf"
	else
	    mkdir /etc/systemd/system/docker.service.d/
            echo "[Service]" | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
            echo "Environnement=\"HTTP_PROXY=http://pcs.ritac.i2:3128\"" | sudo tee --append /etc/systemd/system/docker.service.d/http-proxy.conf
            sudo service docker restart
        fi	    

else

echo "

Options :

    - --create-debian : par defaut creer 2 conteneurs debian (sinon préciser le chiffre en argument)

    - --create-centos : par defaut creer 2 conteneurs centos (sinon preciser le chiffre en argument)
    
    - --drop : supprimer tous les conteneurs que vous avez cree (uniquement ceux commencant par votre nom de user)

    - --infos : caracteristiques des conteneurs (IP, nom, user, ...)

    - --start : demarre tous les conteneurs (start) et le service docker si celui-ci n est pas demarre

    - --proxy : pour ajouter le proxycs dans la conf docker de maniere a faire des push et des pull

    - --ansible : deploiement arborescence ansible

"
fi
